package arrays;

public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlen = {1,2,3,4,42};
		String convertedArray;
		
		convertedArray = convertArrayToString(zahlen);
		System.out.println(convertedArray);

	}
	
	public static String convertArrayToString(int[] zahlen) {
		String converted = "";
		
		for(int i=0; i<zahlen.length; i++) {
			converted += zahlen[i];	
			if((zahlen.length-i)!=1)
			converted += ",";
		}
		return converted;
	}

}
