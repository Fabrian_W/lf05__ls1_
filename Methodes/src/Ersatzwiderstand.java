		/*
		 * "AB Programmier�bungen zu Methoden �berwiegend (Schwierigkeitsgrad 2)"
		 * Aufgabe 3 und 4
		 * 
		 * */

import java.util.Scanner;

public class Ersatzwiderstand {
	
	public static void main(String[] args) {
		
	//Eingabe
	double r1 = liesDouble("Gib den ersten Widerstand an z.B. 3,5 f�r 3,5 Ohm");
	double r2 = liesDouble("Gib den zweiten Widerstand an z.B. 1,0 f�r 1 Ohm");
	//Verarbeitung
	double reihenschaltung = reihenschaltung(r1, r2);
	double parallelschaltung = parallelschaltung (r1, r2);
	//Ausgabe
	ergebnisAusgabe(r1, r2, reihenschaltung, parallelschaltung);
	
		
		
	}

	
	public static void ergebnisAusgabe(double r1, double r2, double reihe, double parallel) {
		System.out.println("Widerstand 1: "+r1+" Ohm \nWiderstand 2: "+r2+" Ohm \nErsatzwiderstand Reihenschaltung: "+reihe+" Ohm \nErsatzwiderstand Parallelschaltung: "+parallel+" Ohm" );
	}

	public static double reihenschaltung (double r1, double r2) {
		double inReihe = r1 + r2;
		return inReihe;
	}
	
	public static double parallelschaltung (double r1, double r2) {
		double parallel = 1/((1/r1)+(1/r2));
		return parallel;
	}
	
	//integer einlesen mit Benutzer definierter Aufforderung (text)
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double eingabeDouble = myScanner.nextDouble();
		return eingabeDouble;
	}

}