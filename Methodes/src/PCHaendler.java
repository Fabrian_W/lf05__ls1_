		/*
		 * "AB Programmierübungen zu Methoden überwiegend (Schwierigkeitsgrad 2)"
		 * Aufgabe 2
		 * 
		 * */

import java.util.Scanner;
	
public class PCHaendler {
		
	public static void main(String[] args) {
				
		//Eingabe
		String artikel = liesString("Was möchten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double nettopreis = liesDouble("Geben Sie den Nettopreis ein:");
		int mwst =  liesInt("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		//Verarbeitung
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, nettopreis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		//Ausgabe
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
			
	}
	
	
	//String einlesen mit Benutzer definierter Aufforderung (text)
	public static String liesString(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String eingabeString = myScanner.next();
		return eingabeString;
	}
	
	//integer einlesen mit Benutzer definierter Aufforderung (text)
	public static int liesInt(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int eingabeInt = myScanner.nextInt();
		return eingabeInt;
	}
	
	//double einlesen mit Benutzer definierter Aufforderung (text)
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double eingabeDouble = myScanner.nextDouble();
		return eingabeDouble;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double ergebnis = anzahl * nettopreis;
		return ergebnis;
	}
		
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double mwstBetrag = (nettogesamtpreis * (mwst/100));
		double gesamtBrutto = nettogesamtpreis + mwstBetrag;
		return gesamtBrutto;	
	}
	
	//Bon
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}

	
	