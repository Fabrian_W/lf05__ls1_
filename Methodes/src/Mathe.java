import java.util.Scanner;

/*
		 * "AB Programmierübungen zu Methoden überwiegend (Schwierigkeitsgrad 2)"
		 * Aufgabe 5
		 * 
		 * */
public class Mathe {

	public static void main(String[] args) {
	
		//Eingabe
		double x = liesDouble("Gib eine Zahl ein (z.B. 2,5)");
		//Verarbeitung
		double quadriert = quadrat(x);
		//Ausgabe
		ausgabe(x,quadriert);

	}
	//ausgabe
	public static void ausgabe(double x,double quadriert) {
		System.out.println("Das Quadrat von " + x + " ist " + quadriert);
	}
	
	//x^2 berechnen
	public static double quadrat(double x) {
		double quadriert = x*x;
		return quadriert;
	}
	
	//eingabe mit default text
	public static double liesDouble(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double eingabeDouble = myScanner.nextDouble();
		return eingabeDouble;
	}

}
