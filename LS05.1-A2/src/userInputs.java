import java.util.Scanner; // Import der Klasse Scanner 

public class userInputs {

	public static void main(String[] args) {
		
		// Create new Scanner-object "myScanner"     
	    Scanner myScanner = new Scanner(System.in);  
	    
	    //request user input (name)
	    System.out.print("Greatings User! May I ask you for your name? Please type it in now: ");
	    String userName = myScanner.nextLine();
	    
	    //request user input (age)
	    System.out.print("Hey " + userName + " , it's nice to meet you! I don't want to appear rude, but can you give me your age too? Please type it in here: ");
	    int userAge = myScanner.nextInt();
	    
	    //output of userName and userAge
	    System.out.print("It was a pleasure to talk to you " + userName + " (" + userAge + ")");
	    
	    //close scanner
	    myScanner.close(); 
	    
	}

}
