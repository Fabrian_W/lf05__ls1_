import java.util.Scanner; // Import der Klasse Scanner 
 
public class rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    int zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    int zahl2 = myScanner.nextInt();  
     
      
    // wird der Variable ergebnis zugewiesen. 
    int ergebnis = zahl1 + zahl2;  
     
    
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
    
    //multiplikation
    int multiSum = zahl1 * zahl2;  
    
    System.out.print("\n\n\nErgebnis der Multiplikation lautet: "); 
    System.out.print(zahl1 + " * " + zahl2 + " = " + multiSum);
    
    //division
    
    float dotedInt1 = zahl1;
    float dotedInt2 = zahl2;
    float diviSum = dotedInt1 / dotedInt2; 
    
    System.out.print("\n\n\nErgebnis der Division lautet: "); 
    System.out.print(zahl1 + " / " + zahl2 + " = " + diviSum);
    
    //subtraktion
    
    int subSum = zahl1 - zahl2; 
    
    System.out.print("\n\n\nErgebnis der Subtraktion lautet: "); 
    System.out.print(zahl1 + " - " + zahl2 + " = " + subSum);
    
 
    myScanner.close(); 
     
  }    
} 