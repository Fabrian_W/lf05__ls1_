
public class Ausgabe {

	public static void main(String[] args) {

		String br = "----------------------------";
		
				//Das ist ein Beispielsatz. Ein Beispielsatz ist das. 
		System.out.print("Das ist ein Beispielsatz. ");
		System.out.println("Ein Beispielsatz ist das. ");
		
		System.out.println(br);  //adds a bookmark 
		
		//Das ist ein �Beispielsatz�.  
		//Ein Beispielsatz ist das. 
		System.out.println("Das ist ein \"Beispielsatz.\"\nEin Beispielsatz ist das.");
		
		System.out.println(br);  //adds a bookmark 
		
		System.out.println("      *      \n     ***     \n    *****    \n   *******   \n  *********  \n *********** \n*************\n    ***    \n    ***    ");
		
		System.out.println(br);  //adds a bookmark 
		
		double a = 22.4234234; 	//22,42
		double b = 111.2222; 	//111,22
		double c = 4.0; 		//4,00
		double d = 1000000.551; //1000000.55
		double e = 97.34;		// 97.34
		
		System.out.printf( "|%.2f\n" , a ); 
		System.out.printf( "|%.2f\n" , b ); 
		System.out.printf( "|%.2f\n" , c ); 
		System.out.printf( "|%.2f\n" , d ); 
		System.out.printf( "|%.2f\n" , e ); 
		
		System.out.printf("Name: %-10s, Alter: %-10d, Gehalt: %-10.2f�%n", "Max", 18, 1801.50);
	
		System.out.println("\n" + br); 
		System.out.println("Konsolenausgabe 2 ");  
		System.out.println(br+"\n");  //adds a bookmark 
		
		//Aufg. 1
		System.out.println("Aufgabe 1\n");
		System.out.printf("%4s%-4s\n","*","*");
		System.out.printf("%-4s%4s\n","*","*");
		System.out.printf("%-4s%4s\n","*","*");
		System.out.printf("%4s%-4s\n","*","*");
		
		System.out.println(br+"\n");  //adds a bookmark 
		//Aufg 2
		System.out.println("Aufgabe 2\n");
		System.out.printf("%-5s=%-19s=%4d%n", "0!", " ", 1);
		System.out.printf("%-5s=%-19s=%4d%n", "1!", " 1", 1);
		System.out.printf("%-5s=%-19s=%4d%n", "2!", " 1 * 2", 2);
		System.out.printf("%-5s=%-19s=%4d%n", "3!", " 1 * 2 * 3", 6);
		System.out.printf("%-5s=%-19s=%4d%n", "3!", " 1 * 2 * 3 * 4", 24);
		System.out.printf("%-5s=%-19s=%4d%n", "3!", " 1 * 2 * 3 * 4 * 5", 120);

		
		System.out.println(br+"\n");  //adds a bookmark 
		
		//Aufg. 3
		System.out.println("Aufgabe: 3\n");
		System.out.printf("%-13s%s%10s\n","Fahrenheit", "|", "Celsius");
		System.out.printf("%s", "------------------------\n");
		System.out.printf("%-13d%s%10.2f\n",-20, "|", 28.8889);
		System.out.printf("%-13d%s%10.2f\n",-10, "|", -23.3333);
		System.out.printf("%s%-12d%s%10.2f\n", "+", 0, "|", -17.7778);
		System.out.printf("%s%-12d%s%10.2f\n", "+", 20, "|", -6.6667);
		System.out.printf("%s%-12d%s%10.2f\n", "+", 30, "|", -1.1111);
		
		
	}

}
