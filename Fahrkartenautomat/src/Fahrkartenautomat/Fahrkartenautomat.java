package Fahrkartenautomat;
//Zwischenstand: Implementierung von Ticketpreisen (werte einf�gen) 
import java.util.Scanner;

public class Fahrkartenautomat{
	
    public static void main(String[] args)
    {
   	
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       
       do {
    	   //Bestellung erfassen
    	   zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	   //Eingezahlen Betrag erfassen
    	   eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       
    	   //Fahrkarte drucken
    	   fahrkartenAusgeben();
    	   //R�ckgeld geben
    	   rueckgeldAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
       
    	   warte(3450); //delay, bevor eine neue Bestellung aufgenommen wird in ms \\
       }
       while(true);
       
    }
    
    
    private static void warte(int milisecond){
    	try {
    		Thread.sleep(milisecond);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
        
    private static double[] fahrkartenPreise() {
    	double[] ticketPreise = {0, 2.90,3.30,3.60,1.90,8.60,9.00,9.60,23.50,24.30,24.90};
    	return ticketPreise;
    }
    private static String[] fahrkartenTyp() {
    	String[] ticketTyp	  = {"", "Einzelfahrschein Berlin AB", "Einzelfahrschein Berlin BC", "Einzelfahrschein Berlin ABC", "Kurzstrecke", "Tageskarte Berlin AB", "Tageskarte Berlin BC", "Tageskarte Berlin ABC", "Kleingruppen-Tageskarte Berlin AB", "Kleingruppen-Tageskarte Berlin BC", "Kleingruppen-Tageskarte Berlin ABC", };
    	return ticketTyp;
    }

    //gibt den Gesamtpreis der Tickets zur�ck
    private static double fahrkartenbestellungErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0;
    	int tickets;
    	boolean ticketInputCheck = true;
    	int ticketChoice = 0;
    	boolean bezahlen = true;
    	String[] ticketName = fahrkartenTyp();
    	double[] ticketPreise = fahrkartenPreise();
    	
    	
    	do {
	        //Ticketauswahl
    		
    		//Ticketauswahl Ausgabe
    		System.out.println("W�hlen Sie Ihr Ticket aus!");
    		System.out.println("Auswahlnummer  | Preis |          Ticket");
    		System.out.println("-------------------------------------------------------------");
    		
    		for(int k = 1; k < ticketPreise.length; k++) {
    			System.out.printf("%d \t\t %.2f�  \t  %s \n", k, ticketPreise[k], ticketName[k]);
    		}
	    
    		System.out.println((ticketPreise.length) + " --Bezahlen--");
    	
    		//ticket Menge -> eingabe + �berpr�fung auf validity 
	    	do { 
	    		ticketChoice = tastatur.nextInt();
		    	
	    		    if(ticketChoice != ticketPreise.length)
	    		    	System.out.println("Ihre Wahl: \n" + ticketChoice + " \t" + ticketName[ticketChoice] + "\n");
	    		    else
	    		    	System.out.println("Bestellvorgang wird beendet!");
	    		    
		    		//checks validity
		    		if((ticketChoice>=1) && (ticketChoice<=(ticketPreise.length))) 
		    			{ticketInputCheck = false;}
		    		else
		    			{System.out.println(" >>falsche Eingabe<<");}		    			
	    	}
	    	while(ticketInputCheck);
	    	
	    	
	    	//ticketPreise.length ist die "Bezahl Option"
	    	if(ticketChoice != (ticketPreise.length)) {
		    	ticketInputCheck = true; //is used later again in a do-while loop
		    	
		    	
		        //Eingabe Anzahl der Tickets
		        System.out.print("Anzahl der Tickets: ");   	   
		        do {
			        tickets = tastatur.nextInt();
			        
			      //ung�ltiger Eingabebereich (<1 / >10 Tickets)
			        if((tickets>10) || (tickets<1))     	
			        	System.out.println(" >> W�hlen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.\n");
			        else {                
				        zuZahlenderBetrag += ticketPreise[ticketChoice] * tickets;
				        System.out.printf("\nZwischensumme: %.2f �\n\n", zuZahlenderBetrag);				        			        			        	
				        }
			        
			        ticketInputCheck = false; //exit loop variable
		        }
		        while(ticketInputCheck);		        
	    	}	    
	    	else bezahlen = false;
    	}
	    while(bezahlen);
    	return zuZahlenderBetrag;
    }
    
    
    //gibt den eingezahlten Betrag zur�ck
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
        double eingezahlterGesamtbetrag = 0.0;
        double eingeworfeneMuenze = 42.3141726; //42+ (PI/10) ist eine sehr unwarscheinliche eingabe und wird f�r die erste Eingabepr�fung als Wert gebraucht
        
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
       
        	//verschiedene Ausgaben, es wird zwischen valid und unvalid eingeworfeneMuenze unterschieden
        	if((eingeworfeneMuenze <= 2 && eingeworfeneMuenze >= 0.05) || eingeworfeneMuenze == 42.3141726) {
        		System.out.printf("Noch zu Zahlender Betrag %.2f Euro \n" , (zuZahlenderBetrag - eingezahlterGesamtbetrag));
        		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
        	}
        	else 
        		System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro)!!!!!: ");
     	   
     	   eingeworfeneMuenze = tastatur.nextDouble();
     	   
     	   //check if input is valid
     	   if(eingeworfeneMuenze <= 2 && eingeworfeneMuenze >= 0.05) 
     		  eingezahlterGesamtbetrag += eingeworfeneMuenze;
     	   
     	   
        }
    return eingezahlterGesamtbetrag;
    }
    
    //animierte Fahrscheinausgabe
    private static void fahrkartenAusgeben() {
        // Fahrscheinausgabe 
        // -----------------     
     	   System.out.println("\nFahrschein wird ausgegeben");
     	   
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
 			warte(250);
 	       }
        System.out.println("\n");
    	
    }
    
    //Textausgabe, welche M�nzen als R�ckgeld ausgegeben werden
    private static void rueckgeldAusgeben(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
    	// R�ckgeldberechnung und -Ausgabe
    	int[] coinArray = new int[200];
    	int coinArrayPointer = 0;
        // -------------------------------
        double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(r�ckgabebetrag > 0.0)
        {
     	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f", r�ckgabebetrag);
     	   System.out.println(" EURO \nwird in folgenden M�nzen ausgezahlt:");

            while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
            {
            	coinArray[coinArrayPointer] = 200;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 2.0;
            }
            while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
            {
            	coinArray[coinArrayPointer] = 100;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 1.0;
            }
            while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
            {
	             coinArray[coinArrayPointer] = 50;
	           	 coinArrayPointer++;
	 	         r�ckgabebetrag -= 0.5;
            }
            while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 20;
           	  	coinArrayPointer++;
           	  	r�ckgabebetrag -= 0.2;
            }
            while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 10;
            	coinArrayPointer++;
            	r�ckgabebetrag -= 0.1;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 5;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.05;
            }
            while(r�ckgabebetrag >= 0.02)// 2 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 2;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.02;
            }
            while(r�ckgabebetrag >= 0.01)// 1 CENT-M�nzen
            {
            	coinArray[coinArrayPointer] = 1;
           	 	coinArrayPointer++;
           	 	r�ckgabebetrag -= 0.01;
            }
            coinArray[coinArrayPointer] = 0;
       	 	coinArrayPointer++;
            muenzeAusgeben(coinArray);
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir w�nschen Ihnen eine gute Fahrt.\n"+
                           "______________________________________\n\n");
    }
    
    private static void muenzeAusgeben(int[] x) { //used for testing, for the right Coin size 
		//initialize variables
    	String coinsOutput = "";
    	int coinChoicePointer = 0; //needed to keep track of, where in the array the program is (only 3 Coins at a time a processed a time)    	
		int[] coinChoice = x; //value in Cents (1� == 100 ), if value == 0 -> no output
		String[][][] coinLayoutCache = new String[3][11][5]; //get loaded with coin Layouts
		boolean lastArrayInputReached = true;
		
		
		String[][] twoEuro = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," "," ","2"," "," "," ","*"," "},
								{"*"," "," ","E","U","R","O"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] oneEuro = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," "," ","1"," "," "," ","*"," "},
								{"*"," "," ","E","U","R","O"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] fiftyCent = {{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," ","5","0"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] twentyCent = {{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," ","2","0"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
								};
		String[][] tenCent = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," ","1","0"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] fiveCent = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," "," ","5"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] twoCent = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," "," ","2"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] oneCent = {	{" "," "," ","*"," ","*"," ","*"," "," "," "},
								{" ","*"," "," "," ","1"," "," "," ","*"," "},
								{"*"," "," ","C","E","N","T"," "," "," ","*"},
								{" ","*"," "," "," "," "," "," "," ","*"," "},
								{" "," "," ","*"," ","*"," ","*"," "," "," "}
							};
		String[][] zeroCent = {	{"","","","","","","","","","",""},
								{"","","","","","","","","","",""},
								{"","","","","","","","","","",""},
								{"","","","","","","","","","",""},
								{"","","","","","","","","","",""}
							};
		
		
		//initialize variables end -----------------------------------------------
		//"do-loop" limits Coins per Row, to 3Coins/per
		do {
			coinLayoutCache = new String[3][11][5]; //reset coinLayoutChache for next 3 Coins
			//for-loop fills 3 coin layouts into coinLayoutChache with 3 Values of INPUT-Array, until last value of array is 0
			for(int l = 1; l<=3; l++) {
				if(coinChoice[coinChoicePointer] == 0) {
					lastArrayInputReached = false;
					coinLayoutCache[l-1] = zeroCent;//Load empty Layout for "zero"-option
				}
				else { //fill coinChoiceCache with coin layouts
					switch(coinChoice[coinChoicePointer]) {
					case 200: {coinLayoutCache[l-1] = twoEuro; break;}
					case 100: {coinLayoutCache[l-1] = oneEuro; break;}
					case  50: {coinLayoutCache[l-1] = fiftyCent; break;}
					case  20: {coinLayoutCache[l-1] = twentyCent; break;}
					case  10: {coinLayoutCache[l-1] = tenCent; break;}
					case   5: {coinLayoutCache[l-1] = fiveCent; break;}
					case   2: {coinLayoutCache[l-1] = twoCent; break;}
					case   1: {coinLayoutCache[l-1] = oneCent; break;}
					
					}
				coinChoicePointer++;	
				}
				
			}
			
			//create Coins (in String) | max. 3 Coins in a Row
			
			for(int j = 0; j <= 4; j++) {
				for(int i = 0; i <= 2; i++) {
					for(int k = 0; k <= 10; k++) { 
						coinsOutput += coinLayoutCache[i][j][k];						
					}
				}
				coinsOutput += "\n"; //next Row
			}
			coinsOutput += "\n"; //next Row
		}
		while(lastArrayInputReached);
	
		System.out.println(coinsOutput);
    	
    }
    
}